@echo OFF
for /f %%n in ('dir /ad /b VaultBackup*') do set foldername=%%n
echo foldername = %foldername% > log.txt
set vaultname="no_date"
for /f "tokens=1,2,3,4,5,6,7,8 delims=_ " %%a in ("%foldername%") do set vaultname=%%b_%%c_%%d_%%e_%%f_%%g_%%h
echo vaultname = %vaultname% >> log.txt
move BackupLog.txt "BackupLog_%vaultname%.txt"
