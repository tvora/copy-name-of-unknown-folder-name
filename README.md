# Grab Date Time from VaultBackup folder
If you use the Autodesk Vault backup command, it outputs two things:

1. a folder named VaultBackup_date_time
1. a log file named Backuplog.txt

# Goal

My aim here is to copy _date_time and append it to Backuplog.txt to produce Backuplog_date_time.txt.

# Warning
Use this at your own risk! I am not responsible for you destroying your own Vault backup.

# Credits

I used Drew's solution on [serverfault.com](http://serverfault.com/questions/51860/read-an-unknown-directories-name-copy-to-it) to build this batch
